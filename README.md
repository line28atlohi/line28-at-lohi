Line28 is located in this desirable area of LoHi in Denver, CO at the corner of 16th and Boulder, just across the river from LoDo and Downtown Denver. Close proximity to public transportation or a quick bike ride give you easy access to restaurants, night life, parks, shops, entertainment.

Address: 1560 Boulder St, Denver, CO 80211

Phone: 303-872-5825